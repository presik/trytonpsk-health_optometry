# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, StateTransition
from trytond.report import Report
from trytond.pool import PoolMeta, Pool
from datetime import datetime, timedelta, date
from trytond.transaction import Transaction
from trytond.pyson import Eval, If, In, Get

__all__ = ['PurchaseRequisition', 'KindMount', 'RequisitionDataVission',
           'PurchaseRequisitionForceDraft', 'RequistionPurchases']


class KindMount(ModelSQL, ModelView):
    'Kind Mount'
    __name__ = 'health_optometry.kind_mount'
    _rec_name = 'name'
    name = fields.Char('Name')


# class RequisitionDataVission(ModelSQL, ModelView):
#     'Data Vission'
#     __name__ = 'purchase.requisition.data_vision'
#     _rec_name = 'value'
#     requisition_line = fields.Many2One('purchase.requisition.line', 'Requisition Line',
#         required=True, select=True)
#     item = fields.Selection([
#             ('', ''),
#             ('od', 'OD'),
#             ('oi', 'OI'),
#             ('add', 'ADD'),
#         ], 'Item', select=True, required=True)
#     type_detail = fields.Selection([
#             ('', ''),
#             ('esfera', 'Esfera'),
#             ('cyl', 'C Y L'),
#             ('eje', 'EJE'),
#         ], 'Type Detail', select=True, required=True)
#     value = fields.Char('Value', required=True)


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'

    purchases = fields.Many2Many('purchase.requisition-purchases', 'requisition', 'purchase',
        'Purchases', readonly=True)

    @classmethod
    def __setup__(cls):
        super(PurchaseRequisition, cls).__setup__()
        cls.employee.states['required'] = False




    @classmethod
    @Workflow.transition('approved')
    def approve(cls, requisitions):
        # super(PurchaseRequisition, cls).approve(requisitions)
        Purchase = Pool().get('purchase.purchase')
        RequisitionPurchaseRel = Pool().get('purchase.requisition-purchases')
        for req in requisitions:
            to_create = {}
            for line in req.lines:
                if line.supplier.id not in to_create.keys():
                    address_ = None
                    if line.supplier.addresses:
                        address_ = line.supplier.addresses[0].id
                    to_create[line.supplier.id] = {
                    'party': line.supplier.id,
                    'purchase_date': line.requisition.supply_date,
                    'state': 'draft',
                    'invoice_address': address_,
                    'lines': [('create', [])],
                    }
                to_create[line.supplier.id]['lines'][0][1].append({
                    'product': line.product.id,
                    'unit': line.unit.id or line.product.template.default_uom.id,
                    'quantity':  line.quantity,
                    'unit_price':  line.unit_price,
                    'description':  line.description,
                })
            for value in to_create.values():
                purchase_, = Purchase.create([value])
                new_purchase = {
                    'purchase': purchase_.id,
                    'requisition': req.id,
                }
                RequisitionPurchaseRel.create([new_purchase])


class RequistionPurchases(ModelSQL):
    'Requisition Purchases'
    __name__ = 'purchase.requisition-purchases'
    _table = 'purchase_requisition_purchases_rel'
    purchase = fields.Many2One('purchase.purchase', 'Purchase', ondelete='CASCADE',
        select=True, required=True)
    requisition = fields.Many2One('purchase.requisition', 'Requisition', ondelete='RESTRICT',
        select=True, required=True)


class PurchaseRequisitionForceDraft(Wizard):
    'PurchaseRequisition Force Draft'
    __name__ = 'purchase.requisition.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        id_ = Transaction().context['active_id']
        Purchase = Pool().get('purchase.purchase')

        PurchaseRequisition = Pool().get('purchase.requisition')
        if id_:
            requisition = PurchaseRequisition.browse([id_])
            PurchaseRequisition.write(requisition, {'state': 'draft'})
            for req in requisition:
                purchases = Purchase.search([
                    ('id', 'in', [p.id for p in req.purchases])
                ])
                for p in purchases:
                    if not p.number:
                        Purchase.delete([p])
        return 'end'


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'

    patient = fields.Many2One('party.party', 'Patient', required=True)
    # data_vision = fields.One2Many('purchase.requisition.data_vision', 'requisition_line', 'Data Vision')

    right_esfera = fields.Char('OD Esfera')
    right_cyl = fields.Char('OD Cylindro')
    right_eje = fields.Char('OD Eje')

    left_esfera = fields.Char('OI Esfera')
    left_cyl = fields.Char('OI Cylindro')
    left_eje = fields.Char('OI Eje')

    add_esfera = fields.Char('ADD Esfera')
    add_cyl = fields.Char('ADD Cylindro')
    add_eje = fields.Char('ADD Eje')

    dp_vision = fields.Char('Dp Vission')
    material = fields.Char('Material')
    filters = fields.Char('Filters')
    vifocal_height = fields.Char('Vifocal Height')
    progressive_height = fields.Char('Progresive Height')
    vertical = fields.Char('Vertical')
    kind_mount = fields.Many2One('health_optometry.kind_mount', 'Kind Mount')
    reference_kind = fields.Char('Reference Kind Amount')
    deliver_date = fields.Date('Deliver Date')


class RequisitionReport(Report):
    __name__ = 'purchase.requisition.optometry_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(OptometryReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        _date = date.today()
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['today'] = _date
        return report_context
