# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pool import PoolMeta, Pool
from datetime import datetime, timedelta, date
from trytond.transaction import Transaction

__all__ = ['PatientEvaluation', 'OptometryReport']


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'
# RX in use
    sphere_right = fields.Char('Sphere Right')
    sphere_left = fields.Char('Sphere Left')
    cylinder_right = fields.Char('Cylinder Right')
    cylinder_left = fields.Char('Cylinder Left')
    eje_right = fields.Char('EJE Right')
    eje_left = fields.Char('EJE Left')
    addition_right = fields.Char('Addition Right')
    addition_left = fields.Char('Addition Left')
    rx_lens_type = fields.Char('Rx Lens Type')
# visual acuity
    sc_far_vision_right = fields.Char('No Correction Far Vision Right')
    sc_far_vision_left = fields.Char('No Correction Far Vision Left')
    cc_far_vision_right = fields.Char('With Correction Far Vision Right')
    cc_far_vision_left = fields.Char('With Correction Far Vision Left')
    next_vision_right = fields.Char('Next Vision Right')
    next_vision_left = fields.Char('Next Vision Left')
# extern exam
    extern_exam_right = fields.Char('Extern Exam Right')
    extern_exam_left = fields.Char('Extern Exam Left')
    ophthalmoscopy_right = fields.Char('ophthalmoscopy Right')
    ophthalmoscopy_left = fields.Char('ophthalmoscopy Left')
# ocular motility
    ocular_motility = fields.Text('Ocular Motility')
# keratometry
    keratometry_right = fields.Char('Keratometry Right')
    keratometry_left = fields.Char('Keratometry Left')
# Retinoscopy
    retinoscopy_right = fields.Char(('Retinoscopy Right'))
    retinoscopy_left = fields.Char(('Retinoscopy Left'))
# subjective
    subjective_right = fields.Char(('Subjective Right'))
    subjective_left = fields.Char(('Subjective Left'))
# Cycloplegia
    cycloplegia_right = fields.Char(('Cycloplegia Right'))
    cycloplegia_left = fields.Char(('Cycloplegia Left'))
# Intraocular Pressure
    intraocular_pressure_right = fields.Char(('Intraocular Pressure Right'))
    intraocular_pressure_left = fields.Char(('Intraocular Pressure Left'))
# Test Color
    color_test_right = fields.Char(('Color Test Right'))
    color_test_left = fields.Char(('Color Test Left'))
# Stereopsis
    stereopsis_right = fields.Char(('Stereopsis Right'))
    stereopsis_left = fields.Char(('Stereopsis Left'))
# rx final Far
    sphere_right_final_far = fields.Char('Sphere Right Final')
    sphere_left_final_far = fields.Char('Sphere Left Final ')
    cylinder_right_final_far = fields.Char('Cylinder Right Final')
    cylinder_left_final_far = fields.Char('Cylinder Left Final')
    eje_right_final_far = fields.Char('EJE Right Final')
    eje_left_final_far = fields.Char('EJE Left Final')
    addition_right_final_far = fields.Char('Addition Right Final')
    addition_left_final_far = fields.Char('Addition Left Final')
    va_far_vision_right = fields.Char('Far Vision Right')
    va_far_vision_left = fields.Char('Far Vision Left')
    va_next_vision_right = fields.Char('Next Vision Right')
    va_next_vision_left = fields.Char('Next Vision Left')
# rx final Near
    sphere_right_final_near = fields.Char('Sphere Right Final')
    sphere_left_final_near = fields.Char('Sphere Left Final')
    cylinder_right_final_near = fields.Char('Cylinder Right Final')
    cylinder_left_final_near = fields.Char('Cylinder Left Final')
    eje_right_final_near = fields.Char('EJE Right Final')
    eje_left_final_near = fields.Char('EJE Left Final')
    addition_right_final_near = fields.Char('Addition Right Final')
    addition_left_final_near = fields.Char('Addition Left Final')
    va_far_vision_right_near = fields.Char('Far Vision Right')
    va_far_vision_left_near = fields.Char('Far Vision Left')
    va_next_vision_right_near = fields.Char('Next Vision Right')
    va_next_vision_left_near = fields.Char('Next Vision Left')
# Lens Detail
    pupillary_distance = fields.Char('Pupillary Distance')
    lens_material = fields.Char('Lens Material')
    filter_type = fields.Char('Filter Type')
    lens_type = fields.Char('Lens Type')
# diagnostic Final
    diagnostic = fields.Text('Diagnostic')
    observation = fields.Text('Observation')
    control_appointment = fields.Char('Control Appointment')

class OptometryReport(Report):
    __name__ = 'gnuhealth.patient.evaluation.optometry_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(OptometryReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        _date = date.today()
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['today'] = _date
        return report_context
