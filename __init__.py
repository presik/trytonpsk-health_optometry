

from trytond.pool import Pool
import health
import requisition


def register():
    Pool.register(
        health.PatientEvaluation,
        requisition.KindMount,
        # requisition.RequisitionDataVission,
        requisition.PurchaseRequisition,
        requisition.PurchaseRequisitionLine,
        requisition.RequistionPurchases,
        module='health_optometry', type_='model')
    Pool.register(
        health.OptometryReport,
        module='health_optometry', type_='report')
    Pool.register(
        requisition.PurchaseRequisitionForceDraft,
        module='health_optometry', type_='wizard')
